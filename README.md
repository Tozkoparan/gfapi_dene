# To install electron version

Extract files and directories in the withelectron file to the main project directory.

Download electron

```
npm install --save-dev electron
```

Edit the package.json file

```
"main": "main.js",
"files": [
    ...,
    "main.js",
],
"scripts": {
    ...,
    "start": "electron .",
}
```

Dont forget to insert api\_secret and api_key into the run.js file and configure your preferences.

You can run the program:
```
npm start
```


# Move file into gfapi folder => prototype

Insert api\_secret and api_key into the file.

Then open that directory in cmd or terminal, and run command => 
```
    node just_create.js
```