"use strict";

const GFAPI_KEY = "key"; //process.env.GFAPI_KEY;
const GFAPI_SECRET = "secret"; //process.env.GFAPI_SECRET;

const GfApi = require("./index.js");

const query_sets = [
  {
    name: "Sunbeam",
    id: "material_sunbeam_crystal",
    type: "material",
    price: 75,
    quantity: 5000,
    photo_url: "https://gameflip.com" + "/img/items/fortnite/icon_material.png",
  },
  {
    name: "Brightcore Ore",
    id: "material_brightcore_crystal",
    type: "material",
    price: 75,
    quantity: 5000,
    photo_url: "https://gameflip.com" + "/img/items/fortnite/icon_material.png",
  },
];

async function main() {
  const gfapi = new GfApi(
    GFAPI_KEY,
    {
      secret: GFAPI_SECRET,
      algorithm: "SHA1",
      digits: 6,
      period: 30,
    },
    {
      logLevel: "debug",
    }
  );

  var listings = [];

  while (true) {
    for (var variable of query_sets) {
      let query = {
        name: variable.name,
        description: "Fast delivery!",
        price: variable.price, // price in cents
        tags: [
          // Must use the correct tag for search/filtering to function properly
          `id: ${variable.id}`,
          `type: ${variable.type}`,
          `quantity: ${variable.quantity}`,
          "game: FORTNITE",
        ],
        Platform: "Other",
        upc: GfApi.UPC.FORTNITE,
        platform: GfApi.PLATFORM.OTHER,
        shipping_within_days: GfApi.SHIPPING_WITHIN_DAYS.ONE,
        expire_in_days: GfApi.EXPIRE_IN_DAYS.SEVEN,
        category: GfApi.CATEGORY.INGAME,
        kind: GfApi.KIND.ITEM,
        digital: true,
        digital_region: "none",
        digital_deliverable: "transfer",
        shipping_predefined_package: "None",
        shipping_fee: 0,
        shipping_paid_by: "seller",
      };

      let listing = await gfapi.listing_post(query);
      gfapi
        .upload_photo(listing.id, variable.photo_url, 0)
        .then(() => {
          return gfapi.upload_photo(listing.id, variable.photo_url);
        })
        .then(() => {
          return gfapi.listing_status(listing.id, GfApi.LISTING_STATUS.ONSALE);
        })
        .catch((err) => {
          console.log(err);
        });
      listings.push(listing.id);
      await sleep(3000);
    }
    console.log(listings);
    await sleep(120000);
    
    for(var ids of listings){
        await gfapi.listing_delete(ids).catch((err) => {
        console.log("=== ERROR", err);
      });
      await sleep(3000);
    }
    listings = [];
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

main().catch((err) => {
  console.log("=== ERROR:", err);
});
