"use strict";

const GFAPI_KEY = "api_key"; //process.env.GFAPI_KEY;
const GFAPI_SECRET = "api_secret"; //process.env.GFAPI_SECRET;

const GfApi = require("./index.js");

async function main() {
  const gfapi = new GfApi(
    GFAPI_KEY,
    {
      secret: GFAPI_SECRET,
      algorithm: "SHA1",
      digits: 6,
      period: 30,
    },
    {
      logLevel: "debug",
    }
  );

  let photo_url = "https://gameflip.com" + "/img/items/rocket-league/key.png";
  let photo_file = "key.png";

  let query = {
    name: "Keys | 10x",
    description: "Selling cheap keys!",
    price: 1050, // price in cents
    tags: [
      // Must use the correct tag for search/filtering to function properly
      "id: key",
      "type: Key",
      "quantity: 10x",
    ],
    upc: GfApi.UPC.RL_XONE,
    platform: GfApi.PLATFORM.XONE,
    shipping_within_days: GfApi.SHIPPING_WITHIN_DAYS.ONE,
    expire_in_days: GfApi.EXPIRE_IN_DAYS.SEVEN,
    category: GfApi.CATEGORY.INGAME,
    kind: GfApi.KIND.ITEM,
    digital: true,
    digital_region: "none",
    digital_deliverable: "transfer",
    shipping_predefined_package: "None",
    shipping_fee: 0,
    shipping_paid_by: "seller",
  };

  let listing = await gfapi.listing_post(query);
  gfapi
    .upload_photo(listing.id, photo_url, 0)
    .then(() => {
      return gfapi.upload_photo(listing.id, photo_url);
    })
    .then(() => {
      return gfapi.listing_status(listing.id, GfApi.LISTING_STATUS.ONSALE);
    })
    .catch((err) => {
      console.log(err);
    });
}

main().catch((err) => {
  console.log("=== ERROR:", err);
});
