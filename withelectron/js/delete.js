"use strict";

let delname, delid;

async function main() {
  const gfapi = new GfApi(
    GFAPI_KEY,
    {
      secret: GFAPI_SECRET,
      algorithm: "SHA1",
      digits: 6,
      period: 30,
    },
    {
      logLevel: "debug",
    }
  );

  let query = {
    name: delname,
    tags: [`id: ${id}`],
    Platform: "Other",
    upc: GfApi.UPC.FORTNITE,
    platform: GfApi.PLATFORM.OTHER,
    shipping_within_days: GfApi.SHIPPING_WITHIN_DAYS.ONE,
    expire_in_days: GfApi.EXPIRE_IN_DAYS.SEVEN,
    category: GfApi.CATEGORY.INGAME,
    kind: GfApi.KIND.ITEM,
    digital: true,
    digital_region: "none",
    digital_deliverable: "transfer",
    shipping_predefined_package: "None",
    shipping_fee: 0,
    shipping_paid_by: "seller",
  };
  document.getElementById("deloutput").innerText = ""; //reset
  for (var i = 0; i < 10; i++) {
    sleep(2000);
    let listing = await gfapi.listing_delete(query).catch((err) => {
      document.getElementById("deloutput").innerText += `ERROR: ${err} `;
    });
    // gfapi
    //   .upload_photo(listing.id, photo_url, 0)
    //   .then(() => {
    //     return gfapi.upload_photo(listing.id, photo_url);
    //   })
    //   .then(() => {
    //     return gfapi.listing_status(listing.id, GfApi.LISTING_STATUS.ONSALE);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     document.getElementById("deloutput").innerText += `ERROR: ${err} `;
    //   });
    document.getElementById("deloutput").innerText += `Deleted: ${listing[
      "delname"
    ].toString()} \n`;
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function deleteItemCheck() {
  var checked = document.querySelector('input[name = "delitem"]:checked');

  if (checked != null) {
    return checked.value; //Alert the value of the checked.
  } else {
    return null; //Alert, nothing was checked.
  }
}

document.getElementById("submitDelButton").addEventListener("click", () => {
  if (deleteItemCheck() === "sunbeam") {
    delname = "sunbeam";
    delid = "material_sunbeam_crystal";
  } else if (deleteItemCheck() === "notsunbeam") {
    delname = "notsunbeam";
    delid = "material_sunbeam_crystal";
  } else if (deleteItemCheck() === null) {
    alert("Nothing checked!");
  } else {
    alert("ERROR OCCURED");
  }
  main().catch((err) => {
    console.log("=== ERROR:", err);
  });
});
