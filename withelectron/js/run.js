"use strict";

const GFAPI_KEY = "keyhere"; //process.env.GFAPI_KEY;
const GFAPI_SECRET = "secrethere"; //process.env.GFAPI_SECRET;

const GfApi = require("./index.js");

let name, id, price, type, quantity, photo_file, photo_url;

async function main() {
  const gfapi = new GfApi(
    GFAPI_KEY,
    {
      secret: GFAPI_SECRET,
      algorithm: "SHA1",
      digits: 6,
      period: 30,
    },
    {
      logLevel: "debug",
    }
  );

  let query = {
    name: name,
    description: "Fast delivery!",
    price: price, // price in cents
    tags: [
      // Must use the correct tag for search/filtering to function properly
      `id: ${id}`,
      `type: ${type}`,
      `quantity: ${quantity}`,
      "game: FORTNITE",
    ],
    Platform: "Other",
    upc: GfApi.UPC.FORTNITE,
    platform: GfApi.PLATFORM.OTHER,
    shipping_within_days: GfApi.SHIPPING_WITHIN_DAYS.ONE,
    expire_in_days: GfApi.EXPIRE_IN_DAYS.SEVEN,
    category: GfApi.CATEGORY.INGAME,
    kind: GfApi.KIND.ITEM,
    digital: true,
    digital_region: "none",
    digital_deliverable: "transfer",
    shipping_predefined_package: "None",
    shipping_fee: 0,
    shipping_paid_by: "seller",
  };
  document.getElementById("addoutput").innerText = ""; //reset
  for (var i = 0; i < 10; i++) {
    sleep(2000);
    let listing = await gfapi.listing_post(query);
    gfapi
      .upload_photo(listing.id, photo_url, 0)
      .then(() => {
        return gfapi.upload_photo(listing.id, photo_url);
      })
      .then(() => {
        return gfapi.listing_status(listing.id, GfApi.LISTING_STATUS.ONSALE);
      })
      .catch((err) => {
        console.log(err);
        document.getElementById("addoutput").innerText += `ERROR: ${err} `;
      });
    document.getElementById("addoutput").innerText += `Added: ${listing[
      "name"
    ].toString()} \n`;
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function addItemCheck() {
  var checked = document.querySelector('input[name = "additem"]:checked');

  if (checked != null) {
    return checked.value; //Alert the value of the checked.
  } else {
    return null; //Alert, nothing was checked.
  }
}

document.getElementById("submitAddButton").addEventListener("click", () => {
  if (addItemCheck() === "sunbeam") {
    name = "sunbeam";
    id = "material_sunbeam_crystal";
    type = "material";
    price = 75;
    quantity = 5000;
    photo_url =
      "https://gameflip.com" + "/img/items/fortnite/icon_material.png";
    photo_file = "icon_material.png";
  } else if (addItemCheck() === "notsunbeam") {
    name = "notsunbeam";
    id = "material_sunbeam_crystal";
    type = "material";
    price = 75;
    quantity = 5000;
    photo_url =
      "https://gameflip.com" + "/img/items/fortnite/icon_material.png";
    photo_file = "icon_material.png";
  } else if (addItemCheck() === null) {
    alert("Nothing checked!");
  } else {
    alert("ERROR OCCURED");
  }
  main().catch((err) => {
    console.log("=== ERROR:", err);
  });
});
